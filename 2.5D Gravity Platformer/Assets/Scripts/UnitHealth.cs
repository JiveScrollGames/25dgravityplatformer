﻿using Assets.Scripts.ScriptableObjects.Brains;
using UnityEngine;
using UnityEngine.Events;

public class UnitHealth : MonoBehaviour {

    public FloatVariable HP;

    public FloatReference StartingHP;
    public UnityEvent DamageEvent;
    public UnityEvent DeathEvent;

    private void Start()
    {
        HP.Value = StartingHP.Value;
    }

    private void OnTriggerEnter(Collider other)
    {
        DamageDealer damage = other.gameObject.GetComponent<DamageDealer>();
        
        if (damage == null)
        {
            damage = other.gameObject.GetComponentInParent<DamageDealer>();
        }

        if (damage != null)
        {
            HP.Value -= damage.DamageAmount;
            DamageEvent.Invoke();

            CharacterThinker enemyObject = other.GetComponent<CharacterThinker>();
            if (enemyObject)
            {
                if (enemyObject.brain.GetName() == "BulletBrain")
                {
                    Destroy(other.gameObject);
                }
            }
        }

        if (HP.Value <= 0.0f)
        {
            DeathEvent.Invoke();
        }
    }
}
