﻿using UnityEngine;

public class ObjectFollower : MonoBehaviour {

    public GameObject ObjectToFollow;
    public Vector3 FollowDistance;
    public bool XAxisOnly = false;

    private void Start()
    {
        if (ObjectToFollow == null)
        {
            ObjectToFollow = FindObjectOfType<Player>().gameObject;
        }
    }
    // Update is called once per frame
    void Update () {
        if (XAxisOnly)
        {
            transform.position = new Vector3(ObjectToFollow.transform.position.x + FollowDistance.x, transform.position.y, transform.position.z);
        }
        else
        {
            transform.position = ObjectToFollow.transform.position + FollowDistance;
        }
	}
}
