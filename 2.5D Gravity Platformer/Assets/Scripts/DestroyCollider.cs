﻿using UnityEngine;

public class DestroyCollider : MonoBehaviour {

    private void OnTriggerEnter(Collider collider)
    {
        Destroy(collider.gameObject);
    }
}
