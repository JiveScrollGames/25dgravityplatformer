﻿namespace Assets.Scripts
{
    [System.Serializable]
    public class SaveData
    {
        public float highScore = 0;
        public int musicVolume = 10;
        public int sfxVolume = 10;
        public int selectedCharacterIndex = 0;
    }
}
