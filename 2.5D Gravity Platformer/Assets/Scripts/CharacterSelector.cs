﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelector : MonoBehaviour {

    public FloatVariable selectedCharacter;
    public Texture2D[] characterImages;
    public RawImage characterImageTarget;
    public GameEvent saveData;

    public void SetSelectedCharacter(int selection)
    {
        selectedCharacter.Value = selection;

        if (characterImageTarget != null && characterImages[(int)selectedCharacter.Value] != null)
        {
            characterImageTarget.texture = characterImages[(int)selectedCharacter.Value];
        }

        saveData.Raise();
    }

    public void SetSelectedCharacterFromSaveData()
    {
        GetComponent<Dropdown>().value = (int)selectedCharacter.Value;

        if (characterImageTarget != null && characterImages[(int)selectedCharacter.Value] != null)
        {
            characterImageTarget.texture = characterImages[(int)selectedCharacter.Value];
        }
    }
}
