﻿using UnityEngine;

public class EnemyShooting : MonoBehaviour {

    public GameObject projectile;
    private float? lastShotTime;
    private Gun gun;
    private CharacterThinker character;
    private MusicPlayer audioPlayer;

    public void Start()
    {
        gun = GetComponentInChildren<Gun>();
        character = GetComponent<CharacterThinker>();
        audioPlayer = GetComponent<MusicPlayer>();
    }

    internal void CheckFire(float delaySeconds)
    {
        if (lastShotTime == null || lastShotTime + delaySeconds < Time.time)
        {
            lastShotTime = Time.time;
            Shoot();
        }
    }

    internal void Shoot()
    {
        if (character.animator != null)
        {
            character.animator.SetTrigger("Attack");
        }

        if (audioPlayer)
        {
            audioPlayer.PlayAudioEvent();
        }

        if (gun)
        {
            Instantiate(projectile, gun.transform);
        }
        else
        {
            Instantiate(projectile, transform);      
        }
    }
}
