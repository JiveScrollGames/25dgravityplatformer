﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSetter : MonoBehaviour {

    public IntVariable musicVolume;
    public IntVariable sfxVolume;
    public GameEvent saveData;

    public void SetMusicVolumeFromSlider(Slider slider)
    {
        SetMusicVolume((int)slider.value);
        saveData.Raise();
    }

    public void SetSFXVolumeFromSlider(Slider slider)
    {
        SetSfxVolume((int)slider.value);
        saveData.Raise();
    }

    public void SetMusicVolume(int volume)
    {
        musicVolume.Value = volume;
    }

    public void SetSfxVolume(int volume)
    {
        sfxVolume.Value = volume;
    }
}
