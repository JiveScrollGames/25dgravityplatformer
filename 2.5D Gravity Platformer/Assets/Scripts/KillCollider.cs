﻿using UnityEngine;
using UnityEngine.Events;

public class KillCollider : MonoBehaviour {

    public LevelSelector levelManager;
    public FloatReference currentDistance;
    public UnityEvent killPlayer;

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.GetComponent<Player>())
        {
            killPlayer.Invoke();
        }
    }
}
