﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSetCreator : MonoBehaviour {

    public PlatformSets platformSetCollection;

    public void CreatePlatformSet()
    {
        PlatformSets.PlatformSet platformSet = new PlatformSets.PlatformSet();

        Platform[] platforms = FindObjectsOfType<Platform>();
        Spike[] spikes = FindObjectsOfType<Spike>();
        Enemy[] enemies = FindObjectsOfType<Enemy>();

        List<PlatformSets.Platform> platformList = new List<PlatformSets.Platform>();
        List<PlatformSets.Spike> spikesList = new List<PlatformSets.Spike>();
        List<PlatformSets.Enemy> enemiesList = new List<PlatformSets.Enemy>();

        foreach (Platform p in platforms)
        {
            PlatformSets.Platform pl = new PlatformSets.Platform();
            Transform platformTransform = p.gameObject.transform;

            pl.position = platformTransform.position;
            pl.rotation = platformTransform.eulerAngles;
            pl.scale = platformTransform.localScale;

            platformList.Add(pl);
        }

        foreach (Spike s in spikes)
        {
            PlatformSets.Spike sp = new PlatformSets.Spike();
            Transform spikeTransform = s.gameObject.transform;

            sp.position = spikeTransform.position;
            sp.rotation = spikeTransform.eulerAngles;

            spikesList.Add(sp);
        }

        foreach (Enemy e in enemies)
        {
            PlatformSets.Enemy en = new PlatformSets.Enemy();
            Transform enemyTransform = e.gameObject.transform;

            en.position = enemyTransform.position;
            en.rotation = enemyTransform.eulerAngles;

            CharacterThinker ct = e.GetComponent<CharacterThinker>();

            if (ct != null)
            {
                en.brain = ct.brain;
            }

            enemiesList.Add(en);
        }

        platformSet.platforms = platformList;
        platformSet.spikes = spikesList;
        platformSet.enemies = enemiesList;

        platformSetCollection.platformSet.Add(platformSet);
    }
}
