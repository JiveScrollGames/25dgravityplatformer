﻿using System;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour
{
    public bool highScore = false;
    public FloatReference currentDistance;
    public FloatVariable highScoreValue;
    
	// Use this for initialization
	void Start ()
    {
        if (!highScore)
        {
            GetScore();      
        }
	}

    public void GetHighScore()
    {
        GetComponent<Text>().text = highScoreValue.Value.ToString();
    }

    // Update is called once per frame
    void Update () {
		if (!highScore)
        {
            GetScore();
        }
	}

    void GetScore()
    {
        GetComponent<Text>().text = currentDistance.Value.ToString();
    }
}
