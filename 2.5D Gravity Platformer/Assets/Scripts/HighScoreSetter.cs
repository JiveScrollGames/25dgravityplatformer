﻿using UnityEngine;

public class HighScoreSetter : MonoBehaviour {

    public GameEvent settingsChangedEvent;
    public GameEvent saveDataEvent;
    public FloatVariable currentHighScore;

    public void SetHighScore(FloatReference currentDistance)
    {
        if ((int)currentDistance.Value > (int)currentHighScore.Value)
        {
            currentHighScore.Value = currentDistance.Value;
            settingsChangedEvent.Raise();
            saveDataEvent.Raise();
        }
    }
}
