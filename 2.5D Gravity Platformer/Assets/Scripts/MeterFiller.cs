﻿using UnityEngine;
using UnityEngine.UI;

public class MeterFiller : MonoBehaviour
{
    public FloatReference CurrentValue;
    public FloatReference Min;
    public FloatReference Max;

    public Image Image;

    private void Update()
    {
        Image.fillAmount = Mathf.Clamp01(
            Mathf.InverseLerp(Min, Max, CurrentValue));
    }
}


