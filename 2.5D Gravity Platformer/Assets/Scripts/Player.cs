﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour {

    private Vector3 move;
    private float verticalSpeed;
    private float gravity = 1.0f;
    public UnityEvent PlayerSpawned;
    public UnityEvent PlayerFalling;
    public UnityEvent PlayerLanded;
    public UnityEvent PlayerGravityFlipped;

    private CharacterController characterController;
    private CollisionFlags collisionFlags;
    private float staticGravityVelocity = 0.01f; //Used to ensure collision flags are always set correctly when on ground.

    private bool playerFalling = false;
    public FloatReference currentDistance;

    private float movementSpeed = 12f;
    private bool gravityFlipped = false;
    private float stuckDelaySeconds = 0.5f;
    public DateTime lastMovedTime;

    // Use this for initialization
    void Start () {
        characterController = GetComponent<CharacterController>();
        currentDistance.Variable.Value = 0;
        PlayerSpawned.Invoke();
        lastMovedTime = DateTime.Now;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (GroundCheck())
            {
                FlipGravity();
            }
        }
    }

    private void CheckStuck()
    {
        if ((collisionFlags & CollisionFlags.CollidedSides) != 0)
        {
            if (lastMovedTime.AddSeconds(stuckDelaySeconds) < DateTime.Now)
            {
                //Apply small nudge to side collisions if character is stuck to platform side
                transform.position += new Vector3(0, verticalSpeed / 5, 0);
            }
        }
    }

    private void CheckFalling()
    {
        if (playerFalling)
        {
            if (GroundCheck())
            {
                PlayerLanded.Invoke();
                playerFalling = false;
            }
        }
        else
        {
            if (!GroundCheck())
            {
                PlayerFalling.Invoke();
                playerFalling = true;
            }
        }
    }

    private void FlipGravity()
    {
        if(gravityFlipped)
        {
            transform.eulerAngles = Vector3.zero;
        }
        else
        {
            transform.eulerAngles = new Vector3(180f, 0f, 0f);
        }
        gravityFlipped = !gravityFlipped;
        PlayerGravityFlipped.Invoke();
    }

    private void FixedUpdate()
    {
        CheckFalling();
        SetGravity();
        Vector3 downwardPull = new Vector3(0, verticalSpeed, 0);

        move = new Vector3(1f, 0, 0);
        move.x = move.x * Time.fixedDeltaTime * movementSpeed;
        move += downwardPull;

        if (currentDistance.Variable.Value != (int)transform.position.x) //Player hasn't moved this frame. Likely stuck
        {
            lastMovedTime = DateTime.Now;
        }

        currentDistance.Variable.Value = (int)transform.position.x;

        CheckStuck();
        Move(move);
    }

    private void Move(Vector3 move)
    {
        collisionFlags = characterController.Move(move);
    }

    private bool GroundCheck()
    {
        if (gravityFlipped)
        {
            return (collisionFlags & CollisionFlags.CollidedAbove) != 0;            
        }

        return (collisionFlags & CollisionFlags.CollidedBelow) != 0; //No collisions returned from Character Controller and no collisions below
    }

    private void SetGravity()
    {
        if (GroundCheck())
        {
            if (gravityFlipped)
            {
                verticalSpeed = staticGravityVelocity;
            }
            else
            {
                verticalSpeed = -staticGravityVelocity;
            }
        }
        else
        {
            if (gravityFlipped)
            {
                verticalSpeed += gravity * Time.fixedDeltaTime;
            }
            else
            {
                verticalSpeed -= gravity * Time.fixedDeltaTime;
            }
        }
    }

}
