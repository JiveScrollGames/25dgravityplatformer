﻿using Assets.Scripts;
using UnityEngine;

public class TestPlatformGenerator : PlatformGenerator
{
    public int platformToTest = 0;

    public override void SpawnPlatformSet()
    {
        Vector3 platformLocation = new Vector3(platformSetSize * platformSetsSpawned, 0, 0);
        platformSetsSpawned++;

        int platformChoice = platformToTest;

        foreach (PlatformSets.Platform p in platformsCollection.platformSet[platformChoice].platforms)
        {
            var newPlatform = Instantiate(platform, new Vector3(p.position.x, p.position.y, p.position.z) + platformLocation, Quaternion.identity);
            newPlatform.transform.localScale = new Vector3(p.scale.x, p.scale.y, p.scale.z);

            if (platformMaterials != null)
            {
                MeshRenderer meshRenderer = newPlatform.GetComponent<MeshRenderer>();
                if (meshRenderer)
                {
                    meshRenderer.material = platformMaterials.materials[materialChoice];
                    meshRenderer.material.mainTextureScale = new Vector2(p.scale.x, 1);
                }
            }


        }

        foreach (PlatformSets.Spike s in platformsCollection.platformSet[platformChoice].spikes)
        {
            var newSpike = Instantiate(spike, s.position + platformLocation, Quaternion.identity);
            newSpike.transform.Rotate(s.rotation);
        }

        foreach (PlatformSets.Enemy e in platformsCollection.platformSet[platformChoice].enemies)
        {
            GameObject newEnemy;

            if (e.brain.GetName() == "WalkerBrain")
            {
                newEnemy = Instantiate(walkerEnemy, e.position + platformLocation, Quaternion.identity);
                newEnemy.transform.Rotate(e.rotation);
            }
            else if (e.brain.GetName() == "TurretBrain")
            {
                newEnemy = Instantiate(turretEnemy, e.position + platformLocation, Quaternion.identity);
                newEnemy.transform.Rotate(e.rotation);
            }
            else
            {
                newEnemy = Instantiate(enemy, e.position + platformLocation, Quaternion.identity);
                newEnemy.transform.Rotate(e.rotation);
            }

            CharacterThinker enemyThinker = newEnemy.GetComponent<CharacterThinker>();
            if (enemyThinker)
            {
                enemyThinker.brain = e.brain;
            }
        }
    }
}

