﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacterInitializer : MonoBehaviour {

    private Player player;
    public GameObject[] characters;
    public FloatVariable selectedCharacterID;

    //Awake used to Instantiate Player, Avoids race conditions with Camera/Object follower which need to find object of type player
	void Awake ()
    {
        if (selectedCharacterID != null)
        {
            int characterID = (int)selectedCharacterID.Value;

            Instantiate(characters[characterID], characters[characterID].transform.position, Quaternion.identity, transform);
        }
        else
        {
            Instantiate(characters[0], characters[0].transform.position, Quaternion.identity, transform);
        }
    }
}
