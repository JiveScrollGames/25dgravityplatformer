﻿using UnityEngine;

public class PlayerDeathDestroyer : MonoBehaviour {

    public void DestroyEnemies()
    {
        Enemy[] enemies = FindObjectsOfType<Enemy>();

        foreach (Enemy enemy in enemies)
        {
            Destroy(enemy.gameObject);
        }
    }
}
