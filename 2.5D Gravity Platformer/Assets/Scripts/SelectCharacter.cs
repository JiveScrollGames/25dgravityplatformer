﻿using UnityEngine;
using UnityEngine.UI;

public class SelectCharacter : MonoBehaviour {

    public FloatVariable characterSelected;

    public void ChooseCharacter(int characterChoice)
    {
        characterSelected.Value = characterChoice;
        Text selectedText = GetComponentInChildren<Text>();
        selectedText.enabled = true;
    }
}
