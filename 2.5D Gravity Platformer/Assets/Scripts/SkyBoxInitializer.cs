﻿using UnityEngine;

public class SkyBoxInitializer : MonoBehaviour {

    public RandomMaterial randomMaterials;

    // Use this for initialization
    void Start () {
        RenderSettings.skybox = randomMaterials.SelectRandomMaterial();
    }
}
