﻿using UnityEngine;

[CreateAssetMenu(menuName = "Visual Collections/Random Materials")]
public class RandomMaterial : ScriptableObject
{
    public Material[] materials;

    public Material SelectRandomMaterial()
    {
        return materials[Random.Range(0, materials.Length)];
    }
}

