﻿using UnityEngine;

public class CharacterThinker : MonoBehaviour
{
    public CharacterBrain brain;
    public GameObject target;
    internal Animator animator;
    internal MusicPlayer audioPlayer;

    private bool audioTriggered = false;

    private void Start()
    {
        animator = GetComponentInChildren<Animator>();
        audioPlayer = GetComponent<MusicPlayer>();
    }
    void Update()
    {
        brain.Think(this);
    }

    public void PlayOneShotAudio()
    {
        if (!audioTriggered)
        {
            audioPlayer.PlayAudioEvent();
            audioTriggered = true;
        }
    }
}