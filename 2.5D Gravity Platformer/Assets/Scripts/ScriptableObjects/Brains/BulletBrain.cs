﻿using UnityEngine;

namespace Assets.Scripts.ScriptableObjects.Brains
{
    [CreateAssetMenu(menuName = "Brains/Bullet")]
    class BulletBrain : CharacterBrain
    {
        private Vector3 FiringDirection = new Vector3(-1f, 0f, 0f);
        public float speedMultiplier;

        public override string GetName()
        {
            return "BulletBrain";
        }

        public override void Think(CharacterThinker character)
        {
            character.transform.position += FiringDirection * Time.deltaTime * speedMultiplier;
        }
    }
}
