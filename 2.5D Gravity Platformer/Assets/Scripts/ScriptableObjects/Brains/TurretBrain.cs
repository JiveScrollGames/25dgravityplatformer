﻿using System;
using UnityEngine;

namespace Assets.Scripts.ScriptableObjects.Brains
{
    [CreateAssetMenu(menuName = "Brains/Turret")]
    class TurretBrain : CharacterBrain
    {
        public float shootingDelay; 
        [MinMaxRange(5, 50)]
        public RangedFloat firingRange;


        public override string GetName()
        {
            return "TurretBrain";
        }

        public override void Think(CharacterThinker character)
        {
            if (character.target == null && FindObjectOfType<Player>())
            {
                character.target = FindObjectOfType<Player>().gameObject;
            }

            if (character.target)
            {
                float targetPos = character.target.transform.position.x;
                float myPos = character.transform.position.x;

                if (targetPos + firingRange.maxValue >= myPos && targetPos + firingRange.minValue <= myPos) //Target within firingRange Boundaries
                {
                    EnemyShooting shooting = character.GetComponent<EnemyShooting>();
                    shooting.CheckFire(shootingDelay);
                }
            }
        }
    }
}
