﻿using UnityEngine;

namespace Assets.Scripts.ScriptableObjects.Brains
{
    [CreateAssetMenu(menuName = "Brains/Walker")]
    class WalkerBrain : CharacterBrain
    {
        public float targetRange;
        public bool hasTriggered = false;

        public float speedMultiplier;

        private Vector3 RunningDirection = new Vector3(-1f, 0f, 0f);

        public override string GetName()
        {
            return "WalkerBrain";
        }

        public override void Think(CharacterThinker character)
        {
            if (character.target == null && FindObjectOfType<Player>())
            {
                character.target = FindObjectOfType<Player>().gameObject;
            }

            if (character.target)
            {
                if (character.target.transform.position.x + targetRange >= character.transform.position.x)
                {
                    if (character.animator != null)
                    {                           
                        character.animator.SetTrigger("Run");
                    }

                    character.PlayOneShotAudio();

                    character.transform.position += RunningDirection * Time.deltaTime * speedMultiplier;
                }
            }
        }
    }
}
