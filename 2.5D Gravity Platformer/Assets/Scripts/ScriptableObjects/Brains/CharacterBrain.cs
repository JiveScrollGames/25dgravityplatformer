﻿using UnityEngine;

public abstract class CharacterBrain : ScriptableObject
{
    public virtual string GetName()
    {
        return "CharacterBrain";
    }
    public virtual void Initialize(CharacterThinker character) { }
    public abstract void Think(CharacterThinker character);
}
