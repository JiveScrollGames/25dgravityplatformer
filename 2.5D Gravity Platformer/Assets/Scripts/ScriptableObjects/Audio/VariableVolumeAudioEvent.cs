﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

namespace Assets.Scripts.ScriptableObjects
{
    [CreateAssetMenu(menuName = "Audio Events/Variable Volume")]
    class VariableVolumeAudioEvent : AudioEvent
    {
        public VariableVolumeAudioClip[] clips;

        public override void Play(AudioSource source, float settingsVolume)
        {
            if (clips.Length == 0) return;

            VariableVolumeAudioClip clip = clips[Random.Range(0, clips.Length)];
            source.clip = clip.clip;
            source.volume = clip.volume;
            source.Play();
        }
    }
}