﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "Audio Events/Random")]
public class RandomAudioEvent : AudioEvent
{
    public AudioClip[] clips;

    public RangedFloat volume;

    public bool useSettingsVolume = false; 

    [MinMaxRange(0, 2)]
    public RangedFloat pitch;

    public override void Play(AudioSource source, float settingsVolume = 1)
    {
        if (clips.Length == 0) return;

        source.clip = clips[Random.Range(0, clips.Length)];

        if (useSettingsVolume)
        {
            source.volume = settingsVolume;
        }
        else
        {
            source.volume = Random.Range(volume.minValue, volume.maxValue);
        }

        source.pitch = Random.Range(pitch.minValue, pitch.maxValue);
        source.Play();
    }
}