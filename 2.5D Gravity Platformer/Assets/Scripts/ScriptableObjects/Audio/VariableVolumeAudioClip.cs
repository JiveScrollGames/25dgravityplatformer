﻿using UnityEngine;

namespace Assets.Scripts.ScriptableObjects
{
    [CreateAssetMenu(menuName = "Audio Events/Variable Volume Clip")]
    public class VariableVolumeAudioClip : ScriptableObject
    {
        public AudioClip clip;
        public float volume;
    }
}