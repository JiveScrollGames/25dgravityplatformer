﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    [CreateAssetMenu(menuName = "PlatformSet")]
    public class PlatformSets : ScriptableObject
    {
        [Serializable]
        public class Platform
        {
            public Vector3 position;
            public Vector3 rotation;
            public Vector3 scale;
        }

        [Serializable]
        public class Spike
        {
            public Vector3 position;
            public Vector3 rotation;
        }

        [Serializable]
        public class Enemy
        {
            public Vector3 position;
            public Vector3 rotation;
            public CharacterBrain brain;
        }

        [Serializable]
        public class PlatformSet
        {
            public List<Platform> platforms;
            public List<Spike> spikes;
            public List<Enemy> enemies;
        }

        public List<PlatformSet> platformSet;
    }
}
