﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu]
public class FloatReference : ScriptableObject {

    public bool UseConstant = true;
    public float ConstantValue;
    public FloatVariable Variable;

    public float Value
    {
        get
        {
            return UseConstant ? ConstantValue : Variable.Value; 
        }
    }

    public static implicit operator float(FloatReference reference)
    {
        return reference.Value;
    }
}
