﻿using UnityEngine;
using UnityEngine.UI;

public class SliderInitializer : MonoBehaviour {

    public Slider musicSlider;
    public Slider sfxSlider;

    public IntVariable musicVolume;
    public IntVariable sfxVolume;

    public void SetSliderValues()
    {
        musicSlider.value = musicVolume.Value;
        sfxSlider.value = sfxVolume.Value;
    }
}
