﻿using Assets.Scripts;
using System;
using System.IO;
using UnityEngine;

public class SettingsHandler : MonoBehaviour {

    public IntVariable musicVolume;
    public IntVariable sfxVolume;
    public FloatVariable selectedCharacter;
    public FloatVariable highScore;

    private string filePath;
    private SaveData saveData;

    public GameEvent settingsChanged;

    public void Start()
    {
        filePath = Path.Combine(Application.persistentDataPath, "gravityflip-scoring.json");
        LoadDataFromFile();
        SetAudioLevels();
    }

    public void LoadDataFromFile()
    {      
        if (!File.Exists(filePath))
        {
            SaveData initialSaveData = new SaveData();
            File.WriteAllText(filePath, JsonUtility.ToJson(initialSaveData, true));
        }

        string dataAsJson = File.ReadAllText(filePath);
        saveData = JsonUtility.FromJson<SaveData>(dataAsJson);

        musicVolume.Value = saveData.musicVolume;
        sfxVolume.Value = saveData.sfxVolume;
        selectedCharacter.Value = saveData.selectedCharacterIndex;
        highScore.Value = saveData.highScore;

        settingsChanged.Raise();
    }

    public void UpdateSaveDataToFile()
    {
        saveData.musicVolume = musicVolume.Value;
        saveData.sfxVolume = sfxVolume.Value;
        saveData.highScore = highScore.Value;
        saveData.selectedCharacterIndex = (int)selectedCharacter.Value;

        File.WriteAllText(filePath, JsonUtility.ToJson(saveData, true));
    }

    public void SetAudioLevels()
    {
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();

        foreach (AudioSource a in audioSources)
        {
            //If audioSource is looping we assume it is music
            if (a.loop)
            {
                a.volume = 1f / 10f * musicVolume.Value;
            }
            else
            {
                a.volume = 1f / 10f * sfxVolume.Value;
            }
        }
    }

    public void SetAudioLevel(AudioSource audio, bool isMusic)
    {
        if (isMusic)
        {
            audio.volume = 1f / 10f * musicVolume.Value;
        }
        else
        {
            audio.volume = 1f / 10f * sfxVolume.Value;
        }
    }

    public float GetMusicVolume()
    {
        return 1f / 10f * musicVolume.Value;
    }

    public float GetSfxVolume()
    {
        return 1f / 10f * sfxVolume.Value;
    }
}
