﻿using UnityEngine;

public class CharacterAnimator : MonoBehaviour {

    public Animator animator;

    public void SetAnimationTrigger(string name)
    {
        animator.SetTrigger(name);
    }
}
