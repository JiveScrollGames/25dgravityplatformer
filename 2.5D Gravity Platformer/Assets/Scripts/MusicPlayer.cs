﻿using UnityEngine;

public class MusicPlayer : MonoBehaviour {

    public AudioSource musicSource;      
    public AudioEvent musicAudioEvent;
    public SettingsHandler settingsHandler;

    public bool playOnAwake = false;

    private void Awake()
    {
        settingsHandler = FindObjectOfType<SettingsHandler>();

        if (playOnAwake)
        {
            PlayAudioEvent();
        }
    }

    public void PlayAudioEvent()
    { 
        if (musicSource.loop)
        {
            musicAudioEvent.Play(musicSource, settingsHandler.GetMusicVolume());
        }
        else
        {
            musicAudioEvent.Play(musicSource, settingsHandler.GetSfxVolume());
        }
    }

    public void SetAudioEvent(AudioEvent audioEvent)
    {
        musicAudioEvent = audioEvent;
    }
}
