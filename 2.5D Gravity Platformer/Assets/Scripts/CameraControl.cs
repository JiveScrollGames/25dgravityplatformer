﻿using UnityEngine;

public class CameraControl : MonoBehaviour
{

    public GameObject target;
    private Vector3 cameraPos;
    private Vector3 playerPos;
    private Vector3 previousPlayerPos;
    private Vector3 CameraOffset = new Vector3(-12.0f, 0f, 0f);

    void Start()
    {
        cameraPos = transform.position;

        if (target == null)
        {
            target = FindObjectOfType<Player>().gameObject;
        }

        previousPlayerPos = target.transform.position + CameraOffset;
    }


    void LateUpdate()
    {
        if (!target)
        {
            target = FindObjectOfType<Player>().gameObject;
        }

        if (target)
        {
            CameraUpdate();
        }
    }

    void CameraUpdate()
    {
        playerPos = target.transform.position;

        if (playerPos != previousPlayerPos)
        {
            cameraPos = transform.position;

            Vector3 playerPositionDifference = playerPos - previousPlayerPos;

            cameraPos += playerPositionDifference;

            transform.position = cameraPos;
        }

        previousPlayerPos = playerPos;
    }

}